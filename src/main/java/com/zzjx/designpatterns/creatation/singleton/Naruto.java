package com.zzjx.designpatterns.creatation.singleton;

/**
 * 火影只能有一个
 */
public class Naruto {
    private String name;
    private String age;

    //添加volatile 内存屏障 为了防止返回不完全对象实例，或造成多例现象
    //也可以用静态内部类来做，静态内部类调用的时候是不可中断的
    private volatile static Naruto instance;
    private Naruto(){
         System.out.println("新的火影诞生了");
    }
    public static Naruto getOne(){
        //1.第一层判断是如果有，直接返回，效率
        if(instance == null){
            synchronized (Naruto.class){
                //2.第二层判断多线程竞争情况
                if(instance==null){
                    Naruto naruto = new Naruto();
                    instance = naruto;
                }
            }
        }
        return instance;
    }
}

