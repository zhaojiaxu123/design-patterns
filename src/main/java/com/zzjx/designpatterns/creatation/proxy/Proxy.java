package com.zzjx.designpatterns.creatation.proxy;

/**
 * @author zhaojiaxu
 * @date 2021/08/18 14:39
 **/
public class Proxy implements Subject {
    Subject subImp = new RealSubject();

    @Override
    public void doSomething() {
          subImp.doSomething();
          after();
    }

    private void after() {
         System.out.println("Congratulations to IG winning the championship!");
    }
}

