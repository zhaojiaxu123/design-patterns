package com.zzjx.designpatterns.creatation.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author zhaojiaxu
 * @date 2021/08/18 14:47
 **/
public class Proxy1 implements InvocationHandler {
    private Object tar;

    //绑定委托对象并返回代理类
    public Object bind(Object tar) {
        this.tar = tar;
        return Proxy.newProxyInstance(tar.getClass().getClassLoader(), tar.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        //这里可以进行类似AOP的操作
        System.out.println(this.getClass().getSimpleName() + "G2 enters the quarterfinals");//在调用具体函数方法前，执行功能扩展
        result = method.invoke(tar, args);
        System.out.println("EDG lost to AFS");//在调用具体函数方法后，执行功能处理
        return result;
    }
}

