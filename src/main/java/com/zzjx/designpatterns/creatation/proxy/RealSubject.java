package com.zzjx.designpatterns.creatation.proxy;

/**
 * @author zhaojiaxu
 * @date 2021/08/18 14:38
 **/
public class RealSubject implements Subject {
    @Override
    public void doSomething() {
         System.out.println("I'm watching s8 in Korea!");
    }
}

