package com.zzjx.designpatterns.creatation.proxy;

/**
 * @author zhaojiaxu
 * @date 2021/08/18 14:38
 **/
public interface Subject {
    void doSomething();
}

