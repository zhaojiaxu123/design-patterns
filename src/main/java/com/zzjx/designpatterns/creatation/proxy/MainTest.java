package com.zzjx.designpatterns.creatation.proxy;

/**
 * @author zhaojiaxu
 * @date 2021/08/18 14:41
 **/
public class MainTest {
    public static void main(String[] args) {
//        Subject subject = new Proxy();
//        subject.doSomething();

        Proxy1 proxy1 = new Proxy1();
        Subject bind = (Subject) proxy1.bind(new RealSubject());
        bind.doSomething();
    }
}

