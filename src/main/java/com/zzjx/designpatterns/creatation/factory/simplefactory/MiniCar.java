package com.zzjx.designpatterns.creatation.factory.simplefactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:43
 **/
public class MiniCar extends  AbstractCar {
    public  MiniCar(){
        this.engine="单缸柴油发动机";
    }
    @Override
    public void run() {
         System.out.println(engine+"突突突");
    }
}

