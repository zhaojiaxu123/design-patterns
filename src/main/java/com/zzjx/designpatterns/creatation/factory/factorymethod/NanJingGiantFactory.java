package com.zzjx.designpatterns.creatation.factory.factorymethod;

import com.zzjx.designpatterns.creatation.factory.abstractfactory.AbstractFactory;
import com.zzjx.designpatterns.creatation.factory.abstractfactory.AbstractMask;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:38
 **/
public class NanJingGiantFactory extends AbstractGiantFactory {

    @Override
    public AbstractGiant newGiant() {
        return new MiniGiant();
    }
}

