package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:19
 **/
public abstract class AbstractFactory {
    protected abstract AbstractMask newMask();
    //如果抽象工厂需要其他功能，我们需要再定义一个抽象类
   // abstract AbstrackCar newCar();
}

