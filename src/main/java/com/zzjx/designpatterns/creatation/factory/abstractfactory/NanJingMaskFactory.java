package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:22
 **/
public class NanJingMaskFactory extends AbstractFactory {
    @Override
    AbstractMask newMask() {
        return new CommonMask();
    }
}

