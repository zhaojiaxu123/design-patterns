package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:37
 **/
public class BigGiant extends AbstractGiant{
    public BigGiant(){
        this.capacity = "巨型巨人，跑的嘎嘎快..";
    }
    @Override
    public void run() {
         System.out.println(capacity+"咔咔咔....");
    }
}

