package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * 抽象巨人(普通巨人，奇行种，七大巨人)
 * 怎么把一个功能提升一个层次：定义抽象（抽象类，接口）
 * 抽象类，接口  就会有多实现，多实现自然就有多功能
 **/
public abstract class AbstractGiant {
    String capacity;
    public abstract void run();
}

