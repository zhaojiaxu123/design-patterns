package com.zzjx.designpatterns.creatation.factory.simplefactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:44
 **/
public class VanCar extends AbstractCar {
    public VanCar(){
        this.engine = "v8 发动机";
    }
    @Override
    public void run() {
        System.out.println(engine +"嗡嗡嗡");
    }
}

