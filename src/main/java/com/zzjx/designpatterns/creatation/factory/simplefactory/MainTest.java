package com.zzjx.designpatterns.creatation.factory.simplefactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:47
 **/
public class MainTest {
    public static void main(String[] args) {
        WuLinSimpleFactory wuLinSimpleFactory = new WuLinSimpleFactory();
        AbstractCar van = wuLinSimpleFactory.newCar("van");
        AbstractCar mini = wuLinSimpleFactory.newCar("mini");
        AbstractCar zzjx = wuLinSimpleFactory.newCar("zzjx");
        van.run();
        mini.run();
    }
}

