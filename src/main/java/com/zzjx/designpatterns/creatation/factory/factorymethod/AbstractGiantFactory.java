package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:32
 **/
public abstract class AbstractGiantFactory {
    public abstract AbstractGiant newGiant();
    //我能造巨人
}

