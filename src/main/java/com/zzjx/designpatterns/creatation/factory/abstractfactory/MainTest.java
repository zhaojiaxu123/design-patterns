package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:18
 **/
public class MainTest {
    /**
     *
     * @author zhaojiaxu
     * @date 2021/08/16 14:23
     * @param args
     * @return void
     * 根据不同的抽象工厂造不同的产品
     */
    public static void main(String[] args) {

        AbstractFactory nanJingMaskFactory = new NanJingMaskFactory();
        AbstractMask abstractMask = nanJingMaskFactory.newMask();
        abstractMask.protectedMe();
    }
}

