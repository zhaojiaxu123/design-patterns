package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:16
 **/
public class CommonMask extends AbstractMask {
    public CommonMask(){
        price = 1;
    }
    @Override
    public void protectedMe() {
         System.out.println("普通口罩....简单保护....请8小时内做更换");
    }
}

