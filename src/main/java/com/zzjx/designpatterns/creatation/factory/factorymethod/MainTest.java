package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:40
 **/
public class MainTest {
    public static void main(String[] args) {
        AbstractGiantFactory wuHanGiantFactory = new WuHanGiantFactory();
        AbstractGiant abstractGiant = wuHanGiantFactory.newGiant();
        abstractGiant.run();
    }
}

