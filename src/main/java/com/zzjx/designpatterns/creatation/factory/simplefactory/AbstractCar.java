package com.zzjx.designpatterns.creatation.factory.simplefactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:42
 **/
public abstract class AbstractCar {
    String engine;
    public abstract void run();
}

