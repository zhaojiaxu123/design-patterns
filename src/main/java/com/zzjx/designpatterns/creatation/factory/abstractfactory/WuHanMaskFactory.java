package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:21
 **/
public class WuHanMaskFactory extends AbstractFactory {
    @Override
    AbstractMask newMask() {
        return new N95Mask();
    }
}

