package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:40
 **/
public class WuHanGiantFactory extends AbstractGiantFactory {
    @Override
    public AbstractGiant newGiant() {
        return new BigGiant();
    }
}

