package com.zzjx.designpatterns.creatation.factory.simplefactory;

/**
 * 简单工厂
 * 1.产品数量极少
 **/
public class WuLinSimpleFactory {

    public AbstractCar newCar(String type) {
        if ("van".equals(type)) {
            return new VanCar();
        } else if ("mini".equals(type)) {
            return new MiniCar();
        }
        return null;
    }
}

