package com.zzjx.designpatterns.creatation.factory.factorymethod;

/**
 * @author zhaojiaxu
 * @date 2021/08/16 14:35
 **/
public class MiniGiant extends AbstractGiant{
    public MiniGiant(){
        this.capacity = "小型巨人，跑的有点慢";
    }
    @Override
    public void run() {
         System.out.println(capacity+"---> 哒哒哒");
    }
}

