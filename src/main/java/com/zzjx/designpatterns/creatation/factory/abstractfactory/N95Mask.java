package com.zzjx.designpatterns.creatation.factory.abstractfactory;

/**
 * 具体产品
 *
 */
public class N95Mask extends AbstractMask {
    public N95Mask() {
        price = 11;
    }

    @Override
    public void protectedMe() {
        System.out.println("N95口罩...可以预防95%的飞沫传播");
    }
}

