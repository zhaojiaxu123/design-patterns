package com.zzjx.designpatterns.event;

import com.zzjx.designpatterns.event.eventListener.AddEventListener;
import com.zzjx.designpatterns.event.eventListener.DelEventListener;
import com.zzjx.designpatterns.event.eventListener.ModifyEventListener;
import com.zzjx.designpatterns.event.eventSource.EventSource;

/**
 * @author zhaojiaxu
 * @date 2021/08/20 09:58
 **/
public class TestMain {
    public static void main(String[] args) {
        EventSource eventSource = new EventSource();
        eventSource.addDoListener(new AddEventListener());
        eventSource.addDoListener(new DelEventListener());
        eventSource.addDoListener(new ModifyEventListener());

        eventSource.fireAddEvent();
        eventSource.fireModifyEvent();
        eventSource.fireDelEvent();
    }
}

